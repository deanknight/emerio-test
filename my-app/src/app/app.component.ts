import { Component } from '@angular/core';
import { constants } from 'buffer';


var temp = 1
var result = 1  
var letCounts = {};
var maxChar = '';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})

export class AppComponent {
  finalnumber = 0
  finalchar

  kirim1(data) {
    var number
    if (data.numval == null || data.numval == "" || data.numval == undefined) {
      number = 0
    } else {
      number = data.numval
    }
    hitung(number)
    this.finalnumber = result
  }

  kirim2(data) {
  var charval
  
    charval = data.charval
    cari(charval)
    this.finalchar = maxChar
  }
}

function hitung(number) {
  if (temp > number) {
    result = 1
    temp = 1
  }

  while(temp <= number) {
    result = result * temp
    temp = temp + 1
  }

  return result
}

function cari(letter) {
  letCounts = {};
  maxChar = '';
  for(var i = 0; i < letter.length; i++)
  {
      var char = letter[i];
      if(!letCounts[char]){
        letCounts[char] = 0;
      }
        letCounts[char]++;

      if(maxChar == '' || letCounts[char] > letCounts[maxChar]){
        maxChar = char;
      }
  }
  return maxChar
}